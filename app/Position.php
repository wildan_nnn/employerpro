<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Position extends Model
{
    protected $fillable = ['nm_jabatan'];

    public function pEmploy()
    {
    	return $this->hasMany(Employer::class, 'id_jabatan');
    }
}
