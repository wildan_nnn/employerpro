@extends('layout.app')

@section('title')
	Data Pegawai
@endsection

@section('head')
	Data Pegawai
@endsection

@section('content')
	@if ($message = Session::get('success'))
	  	<div class="alert alert-success alert-block">
			<button type="button" class="close" data-dismiss="alert">×</button>	
		  	<strong>{{ $message }}</strong>
	  	</div>
	@endif
	<a href="{{ route('add') }} " class="btn btn-primary">Tambah</a>
	<table class="table table-striped">
		<thead>
			<tr>
				<td>Nama</td>
				<td>Alamat</td>
				<td>Tgl Lahir</td>
				<td>Golongan</td>
				<td>Jabatan</td>
				<td>Dibuat</td>
				<td>Diubah</td>
				<td class="col-md-1">Tools</td>
			</tr>
		</thead>
		<tbody>
			@if(!count($emp) == 0)
				@foreach($emp as $p)
				<tr>
					<td>{{ $p->nama }}</td>
					<td>{{ $p->alamat }}</td>
					<td>{{ $p->tgl_lahir }}</td>
					<td>{{$p->cat->nm_gol}}</td>
					<td>{{ $p->pos->nm_jabatan }}</td>
					<td>{{ $p->created_at }}</td>
					<td>{{ $p->updated_at }}</td>
					<td>
					<form action="{{ route('hapus', $p->id) }}" method="POST">
						@csrf
						<a href="{{ route('edit', $p->id) }}" class="btn btn-warning"><span class="fa fa-pencil"></span></a><button class="btn btn-danger" onclick="return confirm('Anda yakin menghapus ini?')"><span class="fa fa-trash"></span></button>
					</td>
					</form>
				</tr>
				@endforeach
			@else
				<tr>
					<td colspan="6">Tidak ada data</td>
				</tr>
			@endif
		</tbody>
	</table>
	<div>
	</div>
@endsection