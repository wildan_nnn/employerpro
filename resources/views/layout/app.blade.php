<html>
	<head>
		<title>@yield('title') - EmployersPro</title>
		<link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet">
		
		<link href="{{ asset('assets/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
	</head>
	<body>
		@include('layout.navbar')
		<div class='container'>
			<h2>@yield('head')</h2>
			<hr/>
			<div class="panel panel-danger">
				<div class="panel-body">@yield('content')</div>
			</div>
		</div>
	<!-- jQuery -->
    <script src="{{ asset('assets/js/jquery.min.js') }}"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src=" {{ asset('assets/js/bootstrap.min.js') }}"></script>
	</body>
</html>