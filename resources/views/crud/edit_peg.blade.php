@extends('layout.app')

@section('title')
	Pegawai
@endsection

@section('head')
	Edit
@endsection

@section('content')
<form action="{{ route('update',$editem->id) }}" method="POST">
	@csrf
	<div class="form-group col-sm-12">
		<label>Nama</label>
		<input type="text" name="nm" class="form-control" value="{{ $editem->nama }}">
	</div>
	<div class="form-group col-sm-12">
		<label>Alamat</label>
		<textarea name="alamat" class="form-control">{{ $editem->alamat }}</textarea>
	</div>
	<div class="form-row">
		<div class="form-group col-md-4">
			<label>Tgl Lahir</label>
			<input type="date" name="tgllhr" class="form-control" value="{{ $editem->tgl_lahir }}">
		</div>
		<div class="form-group col-md-4">
			<label>Golongan</label>
			<select name="gol" class="form-control">
				<option value="{{ $editem->id_golongan }}">{{ $editem->cat->nm_gol }}</option>
				<option>&mdash; Pilih &mdash;</option>
				@foreach($category as $ca)
				<option value="{{ $ca->id }}">{{ $ca->nm_gol }}</option>
				@endforeach	
			</select>
		</div>
		<div class="form-group col-md-4">
			<label>Jabatan</label>
			<select name="jab" class="form-control">
				<option value="{{ $editem->id_jabatan }}">{{ $editem->pos->nm_jabatan }}</option>
				<option>&mdash; Pilih &mdash;</option>
				@foreach($position as $po)
				<option value="{{ $po->id }}">{{ $po->nm_jabatan }}</option>
				@endforeach	
			</select>
		</div>
	</div>
	<div class="col-sm-12">
		<button class="btn btn-info">Simpan</button><a href="{{ url('admin/pegawai') }}" class="btn btn-warning">Kembali</a>
	</div>
</form>
@endsection