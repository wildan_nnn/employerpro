@extends('layout.app')

@section('title')
	Parser
@endsection

@section('head')
	Edit
@endsection

@section('content')
<form action="{{ url('update/'.$edit->id) }}" method="POST">
	@csrf
	<table class="table table-striped">
		<tr>
			<td>Tanya</td>
			<td><input type="text" name="awal" value="{{ $edit->tanya }}" class="form-control"></td>
		</tr>
		<tr>
			<td>Isi</td>
			<td><textarea name="isi" class="form-control" rows="8">{{ $edit->jawab }}</textarea></td>
		</tr>
		<tr>
			<td></td>
			<td><input type="submit" name="submit" value="Simpan" class="btn btn-info"><a href="{{ url('show') }}" class="btn btn-warning">Kembali</a></td>
		</tr>
	</table>
</form>
@endsection