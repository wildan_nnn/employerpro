@extends('layout.app')

@section('title')
	Pegawai
@endsection

@section('head')
	Tambah
@endsection

@section('content')
<form action="{{ route('save') }}" method="POST">
	@csrf
	<div class="form-group col-sm-12">
		<label>Nama</label>
		<input type="text" name="nm" class="form-control">
	</div>
	<div class="form-group col-sm-12">
		<label>Alamat</label>
		<textarea name="alamat" class="form-control"></textarea>
	</div>
	<div class="form-row">
		<div class="form-group col-md-4">
			<label>Tgl Lahir</label>
			<input type="date" name="tgllhr" class="form-control">
		</div>
		<div class="form-group col-md-4">
			<label>Golongan</label>
			<select name="gol" class="form-control">
				<option>&mdash; Pilih &mdash;</option>
				@foreach($cat as $c)
				<option value="{{ $c->id }}">{{ $c->nm_gol }}</option>
				@endforeach	
			</select>
		</div>
		<div class="form-group col-md-4">
			<label>Jabatan</label>
			<select name="jab" class="form-control">
				<option>&mdash; Pilih &mdash;</option>
				@foreach($pos as $p)
				<option value="{{ $p->id }}">{{ $p->nm_jabatan }}</option>
				@endforeach	
			</select>
		</div>
	</div>
	<div class="col-sm-12">
		<button class="btn btn-info">Simpan</button><a href="{{ url('admin/pegawai') }}" class="btn btn-warning">Kembali</a>
	</div>
</form>
@endsection