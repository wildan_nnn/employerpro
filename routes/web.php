<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('layout.login');
});

// Route::get('show', 'ParserController@index');
// Route::post('delete/{id}', 'ParserController@hapus');
// Route::get('edit/{id}', 'ParserController@tampil');
// Route::post('update/{id}', 'ParserController@edit');


Route::group(['prefix' => 'admin'], function(){
	Route::get('/',function(){ 
		return view('web'); 
	})->name('beranda');
	Route::group(['prefix' => 'pegawai'], function(){
		Route::get('/','EmpController@index')->name('yo');
		Route::get('tambah','EmpController@add')->name('add');
		Route::post('save','EmpController@store')->name('save');
		Route::post('delete/{id}','EmpController@hapus')->name('hapus');
		Route::get('edit/{id}','EmpController@edit')->name('edit');
		Route::post('update/{id}','EmpController@update')->name('update');
	});
});
